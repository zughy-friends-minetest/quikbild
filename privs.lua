local S = core.get_translator("quikbild")

core.register_privilege("quikbild_admin", {
    description = S("With this you can use /quikbild create <arena_name>,/quikbild edit <arena_name>")
})
