local S = core.get_translator("quikbild")

quikbild.languages = {
    en = {
        number = 1,
        icon = "quikbild_lang_english.png",
    },
    it = {
        number = 2,
        icon = "quikbild_lang_italian.png",
    },
    es = {
        number = 3,
        icon = "quikbild_lang_spanish.png",
    },
    fr = {
        number = 4,
        icon = "quikbild_lang_french.png",
    },
    de = {
        number = 5,
        icon = "quikbild_lang_german.png",
    },
}

quikbild.languages_by_number = {}

for k,v in pairs(quikbild.languages) do
    quikbild.languages_by_number[v.number] = {
       code = k,
       icon = v.icon,
    }
end

function quikbild.set_language(player, language_number, send_chat)
    if not quikbild.languages_by_number[language_number] then
        return false
    end
    local p_name = player:get_player_name()
    quikbild.storage:set_string("lang_"..p_name, tostring(language_number))
    local arena = arena_lib.get_arena_by_player(p_name)
    if arena then
        arena.players[p_name].lang = language_number
    end
    if send_chat then
        core.chat_send_player(p_name,core.colorize("#7D7071",">> "..S("Language setting confirmed!")))
    end
    local lang_code = quikbild.languages_by_number[language_number].code
    core.log("action", "[quikbild] Quikbild language of "..p_name.." set to '"..lang_code.."' ("..language_number..")")
    return true
end


-- Show language formspec to player
quikbild.send_lang_fs = function(p_name)
    local formstring = "formspec_version[5]"..
    "size[16.6,3]"..
    "background9[0,0;0,0;quikbild_gui_bg.png;true;6,6]"..
    "style_type[image_button;textcolor=#302C2E;font=normal,bold]"
    local x = 0.6
    for l=1, #quikbild.languages_by_number do
        local lang = quikbild.languages_by_number[l]
        formstring = formstring .. "image_button["..x..",0.6;2.6,1.8;"..lang.icon..";lang_"..lang.code..";"..lang.code..";false;true]"
        x = x + 3.2
    end
    core.show_formspec(p_name, "qb_lang", formstring)
end

-- React on Quikbild language selection formspec
core.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "qb_lang" then return end
    local p_name = player:get_player_name()
    local arena = arena_lib.get_arena_by_player(p_name)
    local setting = 0

    local x = 0.6
    for l=1, #quikbild.languages_by_number do
        local lang = quikbild.languages_by_number[l]
        if fields["lang_"..lang.code] then
            setting = l
            core.close_formspec(p_name, "qb_lang")
            break
        end
    end

    if setting ~= 0 then
        quikbild.set_language(player, setting, true)
    end
end)
